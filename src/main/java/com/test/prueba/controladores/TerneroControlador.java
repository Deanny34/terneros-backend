package com.test.prueba.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.prueba.excepciones.ResourceNotFoundException;
import com.test.prueba.modelo.Ternero;
import com.test.prueba.repositorios.TerneroRepositorio;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TerneroControlador {
    
    @Autowired
    private TerneroRepositorio repositorio;

    @GetMapping("/terneros")
    public List<Ternero> listaTerneros() {
        return repositorio.findAll();
    }

    @PostMapping("/terneros")
    public Ternero guardarTernero(@RequestBody Ternero ternero) {
        return repositorio.save(ternero);
    }

    @GetMapping("/terneros/{id}")
    public ResponseEntity<Ternero> obtenerTerneroId(@PathVariable Integer id) {
        Ternero ternero = repositorio.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No existe el ID: " + id));
        return ResponseEntity.ok(ternero);
    }

    @PutMapping("/terneros/{id}")
    public ResponseEntity<Ternero> actualizarTernero(@PathVariable Integer id, @RequestBody Ternero detallesTernero) {
        Ternero ternero = repositorio.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No existe el ID: " + id));
        
        ternero.setNombre(detallesTernero.getNombre());
        ternero.setPeso(detallesTernero.getPeso());
        ternero.setCosto(detallesTernero.getCosto());
        ternero.setProveedor(detallesTernero.getProveedor());
        ternero.setDescripcion(detallesTernero.getDescripcion());
        ternero.setColor_musculo(detallesTernero.getColor_musculo());
        ternero.setCalidad_marmoleo(detallesTernero.getCalidad_marmoleo());
        ternero.setTipo_grasa(detallesTernero.getTipo_grasa());
        
        Ternero terneroActualizado = repositorio.save(ternero);
        return ResponseEntity.ok(terneroActualizado);
    }

    @PutMapping("/ternero-sensores/{id}")
    public ResponseEntity<Ternero> actualizarTerneroSensores(@PathVariable Integer id, @RequestBody Ternero detallesTernero) {
        Ternero ternero = repositorio.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No existe el ID: " + id));
        
        ternero.setFrecuencia_cardiaca(detallesTernero.getFrecuencia_cardiaca());
        ternero.setFrecuencia_respiratoria(detallesTernero.getFrecuencia_respiratoria());
        ternero.setPresion_sanguinea(detallesTernero.getPresion_sanguinea());
        ternero.setTemperatura(detallesTernero.getTemperatura());
        ternero.setFecha_registro_sensores(detallesTernero.getFecha_registro_sensores());
        ternero.setCuarentena(detallesTernero.getCuarentena());
        
        Ternero terneroActualizado = repositorio.save(ternero);
        return ResponseEntity.ok(terneroActualizado);
    }

    @PutMapping("/ternero-cuarentena/{id}")
    public ResponseEntity<Ternero> actualizarTerneroCuarentena(@PathVariable Integer id, @RequestBody Ternero detallesTernero) {
        Ternero ternero = repositorio.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No existe el ID: " + id));
        
        ternero.setCuarentena(detallesTernero.getCuarentena());
        
        Ternero terneroActualizado = repositorio.save(ternero);
        return ResponseEntity.ok(terneroActualizado);
    }
}
