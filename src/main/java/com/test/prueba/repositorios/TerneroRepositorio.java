package com.test.prueba.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.prueba.modelo.Ternero;

@Repository
public interface TerneroRepositorio extends JpaRepository<Ternero, Integer> {
    
}
