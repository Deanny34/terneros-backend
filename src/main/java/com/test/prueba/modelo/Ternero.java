package com.test.prueba.modelo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;


@Entity
@Table(name="terneros")
public class Ternero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nombre", length = 30, nullable = false)
    private String nombre;

    @Column(name="peso", nullable = true)
    private Float peso;

    @Column(name="costo", nullable = false)
    private Float costo;

    @Column(name="proveedor", length = 50, nullable = false)
    private String proveedor;

    @Column(name="descripcion", length = 200, nullable = true)
    private String descripcion;

    @Column(name="color_musculo", nullable = true)
    private Integer color_musculo;

    @Column(name="calidad_marmoleo", nullable = true)
    private Integer calidad_marmoleo;

    @Column(name="tipo_grasa", nullable = true)
    private Integer tipo_grasa;

    @Column(name="frecuencia_cardiaca", nullable = true)
    private Integer frecuencia_cardiaca;

    @Column(name="frecuencia_respiratoria", nullable = true)
    private Integer frecuencia_respiratoria;

    @Column(name="presion_sanguinea", nullable = true)
    private Integer presion_sanguinea;

    @Column(name="temperatura", nullable = true)
    private Float temperatura;

    @Column(name="fecha_registro_sensores", nullable = true)
    private String fecha_registro_sensores;

    @Column(name="cuarentena", nullable = true)
    private Integer cuarentena;

    public Ternero() { }

    public Ternero(Integer id, String nombre, Float peso, Float costo, String proveedor, String descripcion, Integer color_musculo, Integer calidad_marmoleo, Integer tipo_grasa, Integer frecuencia_cardiaca, Integer frecuencia_respiratoria, Integer presion_sanguinea, Float temperatura, String fecha_registro_sensores, Integer cuarentena) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.peso = peso;
        this.costo = costo;
        this.proveedor = proveedor;
        this.descripcion = descripcion;
        this.color_musculo = color_musculo;
        this.calidad_marmoleo = calidad_marmoleo;
        this.tipo_grasa = tipo_grasa;
        this.cuarentena = cuarentena;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Float getCosto() {
        return costo;
    }

    public void setCosto(Float costo) {
        this.costo = costo;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getColor_musculo() {
        return color_musculo;
    }

    public void setColor_musculo(Integer color_musculo) {
        this.color_musculo = color_musculo;
    }

    public Integer getCalidad_marmoleo() {
        return calidad_marmoleo;
    }

    public void setCalidad_marmoleo(Integer calidad_marmoleo) {
        this.calidad_marmoleo = calidad_marmoleo;
    }

    public Integer getTipo_grasa() {
        return tipo_grasa;
    }

    public void setTipo_grasa(Integer tipo_grasa) {
        this.tipo_grasa = tipo_grasa;
    }

    public Integer getFrecuencia_cardiaca() {
        return frecuencia_cardiaca;
    }

    public void setFrecuencia_cardiaca(Integer frecuencia_cardiaca) {
        this.frecuencia_cardiaca = frecuencia_cardiaca;
    }

    public Integer getFrecuencia_respiratoria() {
        return frecuencia_respiratoria;
    }

    public void setFrecuencia_respiratoria(Integer frecuencia_respiratoria) {
        this.frecuencia_respiratoria = frecuencia_respiratoria;
    }

    public Integer getPresion_sanguinea() {
        return presion_sanguinea;
    }

    public void setPresion_sanguinea(Integer presion_sanguinea) {
        this.presion_sanguinea = presion_sanguinea;
    }

    public Float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public String getFecha_registro_sensores() {
        return fecha_registro_sensores;
    }

    public void setFecha_registro_sensores(String fecha_registro_sensores) {
        this.fecha_registro_sensores = fecha_registro_sensores;
    }

    public Integer getCuarentena() {
        return cuarentena;
    }

    public void setCuarentena(Integer cuarentena) {
        this.cuarentena = cuarentena;
    }
}
