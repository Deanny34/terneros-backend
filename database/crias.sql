-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-01-2024 a las 02:42:55
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `terneros`
--

CREATE TABLE `terneros` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `peso` float(10,2) DEFAULT NULL,
  `costo` float(10,2) NOT NULL,
  `proveedor` varchar(50) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `color_musculo` int(11) DEFAULT 0,
  `calidad_marmoleo` int(11) DEFAULT 0,
  `tipo_grasa` int(11) DEFAULT NULL,
  `frecuencia_cardiaca` int(11) DEFAULT NULL,
  `frecuencia_respiratoria` int(11) DEFAULT NULL,
  `presion_sanguinea` int(11) DEFAULT NULL,
  `temperatura` float(10,2) DEFAULT NULL,
  `fecha_registro_sensores` date DEFAULT NULL,
  `cuarentena` int(4) DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `terneros`
--

INSERT INTO `terneros` (`id`, `nombre`, `peso`, `costo`, `proveedor`, `descripcion`, `color_musculo`, `calidad_marmoleo`, `tipo_grasa`, `frecuencia_cardiaca`, `frecuencia_respiratoria`, `presion_sanguinea`, `temperatura`, `fecha_registro_sensores`, `cuarentena`) VALUES
(1100, 'tony', 15.00, 100.00, '15', 'dhl', 3, 2, 1, 120, 15, 9, 37.99, '2024-01-13', 0),
(1101, 'cony', 30.00, 100.00, 'peso pluma', '100', 6, 3, 2, NULL, NULL, NULL, NULL, NULL, 1),
(1102, 'sofi', 30.00, 35000.00, 'peso pluma', 'ta bonita', 3, 2, 1, 69, 14, 9, 38.00, '2024-01-13', 2),
(1103, 'tony', 453.00, 424.00, 'mex', NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `terneros`
--
ALTER TABLE `terneros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `terneros`
--
ALTER TABLE `terneros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
